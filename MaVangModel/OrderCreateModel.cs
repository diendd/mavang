﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MaVangModel
{
    public class OrderCreateModel
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public List<ProductOrderModel> ProductOrder { get; set; }
    }

    public class ProductOrderModel
    {
        public int IDProduct { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
    }
}
