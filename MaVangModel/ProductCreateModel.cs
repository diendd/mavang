﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using MaVangDatabase.Data;

namespace MaVangModel
{
    public class ProductCreateModel
    {
        [Display(Name ="Tên sản phẩm")]
        [Required(ErrorMessage ="Nhập tên sản phẩm")]
        public string ProductName { get; set; }

        [Display(Name = "Giá sản phẩm")]
        [Required(ErrorMessage = "Nhập giá sản phẩm")]
        public decimal ProductPrice { get; set; }

        [Display(Name = "Mô tả sản phẩm")]
        public string Description { get; set; }

        [Display(Name ="Ảnh Thumbnail")]
        [Required(ErrorMessage = "Chọn ảnh thumbnail cho sản phẩm")]
        public IFormFile Thumbnail { get; set; }

        [Display(Name ="Ảnh sản phẩm")]
        [Required(ErrorMessage = "Chọn ảnh cho sản phẩm")]
        public List<IFormFile> ProductImage { get; set; }

        [Display(Name = "Danh mục sản phẩm")]
        public int ProductCategory { get; set; }

        public IEnumerable<m_category> Categories { get; set; }
        
        
    }
}
