﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MaVangModel
{
    public class ProductDetailModel : ProductBaseModel
    {
        public string Images { get; set; }
        public string Category { get; set; }
        
        public string Description { get; set; }
    }
}
