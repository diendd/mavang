﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MaVangModel
{
    public class ProductBaseModel
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }

        public string ImgThumbnail { get; set; }

        public decimal Price { get; set; }
    }
}
