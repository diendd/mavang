﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MaVangModel
{
    public class ProductListHomePageModel
    {
        public List<ProductListModel> noithatList { get; set; }
        public List<ProductListModel> phukienList { get; set; }
        public List<ProductListModel> ortherList { get; set; }
    }
}
