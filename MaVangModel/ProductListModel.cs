﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MaVangModel
{
    public class ProductListModel
    {
        public string ProductName { get; set; }
        public decimal ProductPrice { get; set; }
        public string ImgThumbnail { get; set; }
        public int ProductID { get; set; }
    }
}
