﻿using MaVangMvc.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaVangMvc.Views.Shared.ViewComponents
{
    public class MiniCartViewComponent : ViewComponent
    {
        public const string CARTKEY = "cart";
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var session = HttpContext.Session;
            var jsonCart = session.GetString(CARTKEY);
            if (jsonCart != null)
            {
                var model = JsonConvert.DeserializeObject<List<CartModel>>(jsonCart);
                decimal totalPrice = 0;
                foreach (var item in model)
                {
                    totalPrice = item.Quantity * item.Product.Price + totalPrice;
                }
                ViewData["TotalPrice"] = totalPrice.ToString("n0");
                return View(model);
            }
            return View(new List<CartModel>());
        }
    }
}
