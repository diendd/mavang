﻿using MaVangMvc.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaVangMvc.Views.Shared.ViewComponents
{
    public class BasketViewComponent : ViewComponent
    {
        public const string CARTKEY = "cart";
            
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var session = HttpContext.Session;
            var jsonCart  = session.GetString(CARTKEY);
            if (jsonCart != null)
            {
                var cartItem = JsonConvert.DeserializeObject<List<CartModel>>(jsonCart);
                var TotalOfQuatity = 0;
                for (int i = 0; i < cartItem.Count; i++)
                {
                    TotalOfQuatity += cartItem[i].Quantity;
                }
                ViewData["CartItemCount"] = TotalOfQuatity;
            }
            return View();
        }
    }
}
