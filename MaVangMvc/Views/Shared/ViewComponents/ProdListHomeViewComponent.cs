﻿using MaVangDatabase.Data;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MaVangModel;

namespace MaVangMvc.Views.Shared.ViewComponents
{
    public class ProdListHomeViewComponent : ViewComponent
    {
        private readonly MaVangDbContext _context;
        public ProdListHomeViewComponent(MaVangDbContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            ProductListHomePageModel model = new ProductListHomePageModel();
            model.noithatList = (from p in _context.m_product
                                 where p.CategoryRefID.Equals(1) && p.DelFlg.Equals(false)
                                 select new ProductListModel
                                 {
                                     ProductName = p.ProductName,
                                     ProductPrice = p.ProductPrice,
                                     ImgThumbnail = p.Thumbnail,
                                     ProductID = p.ID
                                 }).ToList();

            model.phukienList = (from p in _context.m_product
                                 where p.CategoryRefID.Equals(2) && p.DelFlg.Equals(false)
                                 select new ProductListModel
                                 {
                                     ProductName = p.ProductName,
                                     ProductPrice = p.ProductPrice,
                                     ImgThumbnail = p.Thumbnail,
                                     ProductID = p.ID
                                 }).ToList();

            model.ortherList = (from p in _context.m_product
                                where p.CategoryRefID != 1 && p.CategoryRefID != 2 && p.DelFlg.Equals(false)
                                 select new ProductListModel
                                 {
                                     ProductName = p.ProductName,
                                     ProductPrice = p.ProductPrice,
                                     ImgThumbnail = p.Thumbnail,
                                     ProductID = p.ID
                                 }).ToList();

            return View(model);
        }
    }
}
