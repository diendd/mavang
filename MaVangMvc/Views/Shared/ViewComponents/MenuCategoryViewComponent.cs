﻿using MaVangDatabase.Data;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MaVangMvc.Models;
using Microsoft.EntityFrameworkCore;

namespace MaVangMvc.Views.Shared.ViewComponents
{
    public class MenuCategoryViewComponent : ViewComponent
    {
        private readonly MaVangDbContext _context;
        public MenuCategoryViewComponent(MaVangDbContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var item = await GetItemsAsync();
            return View(item);
        }
        
        private Task<List<MenuCategoryModel>> GetItemsAsync()
        {
            return  _context.m_category.Select(c => new MenuCategoryModel { CategoryID = c.ID, CategoryName = c.CategoryName }).ToListAsync();
        }
       
    }
}
