﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MaVangDatabase.Data;
using MaVangModel;
using MaVangMvc.Models;
using MaVangUtility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace MaVangMvc.Controllers
{
    public class ProductController : Controller
    {
        private readonly MaVangDbContext _context;
        public const string CARTKEY = "cart";

        public List<CartModel> GetCartItems()
        {
            var session = HttpContext.Session;
            string jsoncart = session.GetString(CARTKEY);
            if (jsoncart != null)
            {
                return JsonConvert.DeserializeObject<List<CartModel>>(jsoncart);
            }
            return new List<CartModel>();
        }

        public void ClearCart()
        {
            var session = HttpContext.Session;
            session.Remove(CARTKEY);
        }

        public void SaveCartSession(List<CartModel> ls)
        {
            var session = HttpContext.Session;
            string jsonCart = JsonConvert.SerializeObject(ls);
            session.SetString(CARTKEY, jsonCart);
        }

        [Route("addcart/{productid:int}", Name = "addcart")]
        public IActionResult AddToCart([FromRoute] int ProductID)
        {
            var product = from p in _context.m_product
                          where p.ID == ProductID
                          select new ProductBaseModel
                          {
                              ProductID = p.ID,
                              Price = p.ProductPrice,
                              ProductName = p.ProductName,
                              ImgThumbnail = p.Thumbnail
                          };
                            
            if(product == null)
            {
                return NotFound("Khong tim thay san pham");
            }

            var cart = GetCartItems();
            var cartItem = cart.Find(p => p.Product.ProductID == ProductID);
            if(cartItem != null)
            {
                cartItem.Quantity++;
            }
            else
            {
                cart.Add(new CartModel() { Quantity = 1, Product = product.FirstOrDefault() });
            }
            SaveCartSession(cart);

            return RedirectToAction(nameof(Cart));
            //return Redirect(HttpContext.Request.Headers["Referer"]);
        }

        [Route("/cart", Name = "cart")]
        public IActionResult Cart()
        {
            return View(GetCartItems());
        }


        public ProductController(MaVangDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> ProductListByCategory(int CategoryID, int? pageNumber)
        {
            var lstModel = from p in _context.m_product
                       where p.CategoryRefID.Equals(CategoryID)
                       select new ProductListModel
                       {
                           ProductName = p.ProductName,
                           ProductPrice = p.ProductPrice,
                           ImgThumbnail = p.Thumbnail,
                           ProductID = p.ID
                       };

            int pageSize = 12;

            return View(await PaginatedList<ProductListModel>.CreateAsync(lstModel.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        public async Task<IActionResult> ProductDetail(int ProductID)
        {
            var model = from p in _context.m_product
                    join c in _context.m_category on p.CategoryRefID equals c.ID
                    where p.ID.Equals(ProductID) && p.DelFlg.Equals(false)
                    select new ProductDetailModel
                    {
                        ProductID = p.ID,
                        Category = c.CategoryName,
                        Price = p.ProductPrice,
                        Images = p.ProductImages,
                        ProductName = p.ProductName,
                        Description = p.Description
                    };
            return View(await model.FirstOrDefaultAsync());
        }

    }
}
