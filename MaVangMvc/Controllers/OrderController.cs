﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MaVangDatabase.Data;
using MaVangMvc.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using MaVangModel;
namespace MaVangMvc.Controllers
{
    public class OrderController : Controller
    {
        private readonly MaVangDbContext _context;
        public IActionResult Index()
        {
            return View();
        }

        public const string CARTKEY = "cart";

        public List<CartModel> GetCartItems()
        {
            var session = HttpContext.Session;
            string jsoncart = session.GetString(CARTKEY);
            if (jsoncart != null)
            {
                return JsonConvert.DeserializeObject<List<CartModel>>(jsoncart);
            }
            return new List<CartModel>();
        }

        public async Task<IActionResult> Checkout()
        {
            return View(GetCartItems());
        }
        
        [HttpPost]
        public async Task<IActionResult> CreateOrder(OrderCreateModel model)
        {
            if (ModelState.IsValid)
            {
                t_order order = new t_order
                {
                    CustomerName = model.Name,
                    Phone = model.Phone,
                    Address = model.Address,
                    Status = 0
                };
                _context.Add(order);
                await _context.SaveChangesAsync();
            }
            return View();
        }
    }
}
