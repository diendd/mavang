﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MaVangDatabase.Data;
using Microsoft.Extensions.Logging;
using MaVangMvc.Models;
using Microsoft.AspNetCore.Mvc.Filters;

namespace MaVangMvc.Controllers
{
    public class BaseController : Controller
    {
        
        private readonly MaVangDbContext _context;
        public List<MenuCategoryModel> menu;
        public BaseController()
        {

        }
        public BaseController(MaVangDbContext context)
        {
            _context = context;
        }

        public  IActionResult MenuCategory()
        {
            var menuCategory = from c in _context.m_category
                               select new MenuCategoryModel
                               {
                                   CategoryID = c.ID,
                                   CategoryName = c.CategoryName
                               };
            return PartialView("_Menubar", menuCategory);
        }


    }
}
