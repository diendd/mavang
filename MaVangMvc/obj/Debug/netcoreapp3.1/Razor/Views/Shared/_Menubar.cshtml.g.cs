#pragma checksum "D:\Project\MaVangMvc\MaVangMvc\Views\Shared\_Menubar.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b0e2f1f3ea1f94d19afbca45a2df9bdf5f19f614"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared__Menubar), @"mvc.1.0.view", @"/Views/Shared/_Menubar.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Project\MaVangMvc\MaVangMvc\Views\_ViewImports.cshtml"
using MaVangMvc;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Project\MaVangMvc\MaVangMvc\Views\_ViewImports.cshtml"
using MaVangMvc.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b0e2f1f3ea1f94d19afbca45a2df9bdf5f19f614", @"/Views/Shared/_Menubar.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ccdd5ad8c3359477917fd0b0541ce81ecde68c64", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared__Menubar : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<MenuCategoryModel>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"<div class=""main_menu menu_position"">
    <nav>
        <ul>
            <li>
                <a class=""active"" href=""index.html"">Trang Chủ<i class=""fa fa-angle-down""></i></a>
            </li>
            <li class=""mega_items"">
                <a href=""shop.html"">shop<i class=""fa fa-angle-down""></i></a>
                <div class=""mega_menu"">
                    <ul class=""mega_menu_inner"">
                        <li>
                            <a href=""#"">Sản phẩm</a>
                            <ul>
");
            WriteLiteral("                            </ul>\r\n                        </li>\r\n                    </ul>\r\n                </div>\r\n            </li>\r\n            <li><a href=\"contact.html\"> Contact Us</a></li>\r\n        </ul>\r\n    </nav>\r\n</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<MenuCategoryModel>> Html { get; private set; }
    }
}
#pragma warning restore 1591
