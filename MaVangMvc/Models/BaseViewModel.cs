﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaVangMvc.Models
{
    public class BaseViewModel
    {
        public List<MenuCategoryModel> MenuCategoryModels { get; set; }
    }
}
