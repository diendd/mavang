﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaVangMvc.Models
{
    public class MenuCategoryModel
    {
        public string CategoryName { get; set; }
        public int CategoryID { get; set; }
    }
}