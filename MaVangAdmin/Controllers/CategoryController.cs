﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MaVangDatabase.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace MaVangAdmin.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ILogger<CategoryController> _logger;
        private readonly MaVangDbContext _context;

        public CategoryController(ILogger<CategoryController> logger, MaVangDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult CategoryList(string searchString, string currentFilter, int? pageNumber)
        {
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewData["CurrentFillter"] = searchString;
            var category = from m in _context.m_category
                           select m;
            
            return View(category);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public  IActionResult Create(m_category category)
        {
            if (ModelState.IsValid)
            {
                _context.Add(category);
                _context.SaveChanges();
                _logger.LogInformation("Create sucess category ID: " + category.ID);
                return RedirectToAction("CategoryList");
            }
            return View();
            
        }

        public async Task<IActionResult> Delete(int id)
        {
            if (ModelState.IsValid)
            {
                m_category categoryDelete = new m_category() { ID = id };
                _context.Entry(categoryDelete).State = EntityState.Deleted;

                await _context.SaveChangesAsync();
                _logger.LogInformation("Remove sucess category ID: " + categoryDelete.ID);
            }
            return RedirectToAction("CategoryList");
        }
    }
}
