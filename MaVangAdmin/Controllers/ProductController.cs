﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MaVangDatabase.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MaVangUtility;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using MaVangModel;
using MaVangAdmin.Utility;
using Microsoft.AspNetCore.Hosting;

namespace MaVangAdmin.Controllers
{
    public class ProductController : Controller
    {
        private readonly MaVangDbContext _context;
        private readonly ILogger<ProductController> _logger;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public ProductController(MaVangDbContext context, ILogger<ProductController> logger, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _logger = logger;
            _webHostEnvironment = webHostEnvironment;
        }

        public async Task<IActionResult> ProductList(string searchString, string currentFilter, int? pageNumber)
        {
            try
            {
                if(searchString != null)
                {
                    pageNumber = 1;
                }
                else
                {
                    searchString = currentFilter;
                }

                ViewData["CurrentFilter"] = searchString;
                var product = from p in _context.m_product
                              join c in _context.m_category on p.CategoryRefID equals c.ID
                              select p;
                if (!String.IsNullOrEmpty(searchString))
                {
                    product = product.Where(p => p.ProductName.Contains(searchString));
                }
                
                int pageSize = 10;

                return View(await PaginatedList<m_product>.CreateAsync(product.AsNoTracking(), pageNumber ?? 1, pageSize));
            }catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                return View();
            }
        }

        [HttpGet]
        public IActionResult Create()
        {
            ProductCreateModel obj = new ProductCreateModel();
            obj.Categories =  _context.m_category.Where(m => m.DelFlg == false).AsEnumerable();
            return View(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ProductCreateModel model)
        {

            if (ModelState.IsValid)
            {
                string uniqueThumbnailName = UploadedFile.UploadedImage(_webHostEnvironment, model.Thumbnail);
                string productImageName = "";
                for (int i = 0; i < model.ProductImage.Count; i++)
                {
                    productImageName += UploadedFile.UploadedImage(_webHostEnvironment, model.ProductImage[i]) + ";" ;
                }
                m_product prod = new m_product
                {
                    ProductName = model.ProductName,
                    ProductPrice  = model.ProductPrice,
                    ProductImages = productImageName,
                    CategoryRefID = model.ProductCategory,
                    DelFlg = false,
                    Thumbnail = uniqueThumbnailName,
                    Description = model.Description
                };

                _context.Add(prod);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(ProductList));
            }
            model.Categories = _context.m_category.Where(m => m.DelFlg == false).AsEnumerable();
            return View(model);
        }
    }
}
