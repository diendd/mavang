﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MaVangDatabase.Data
{
    public class m_product
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string ProductName { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal ProductPrice { get; set; } 

        public string ProductImages { get; set; }

        public string Thumbnail { get; set; }

        public string Description { get; set; }

        public bool DelFlg { get; set; }

        public int CategoryRefID { get; set; }
        public m_category Category { get; set; }
    }
}
