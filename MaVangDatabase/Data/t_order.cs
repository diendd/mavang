﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MaVangDatabase.Data
{
    public class t_order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set;}

        public string CustomerName { get; set; }

        public string Phone { get; set; }
        public string Address { get; set; }

        public int Status { get; set; } //0: chưa liên hệ, 1: đã liên hệ
    }
}
