﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MaVangDatabase.Data
{
    public class t_order_product
    {
        public int IDOrder { get; set; }
        public int IDProduct { get; set; }

        public decimal ProductPrice { get; set; }
    }
}
