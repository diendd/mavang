﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MaVangDatabase.Data
{
    public static class DbInitializer
    {
        public static void Initialize(MaVangDbContext context)
        {
            context.Database.EnsureCreated();
        }
    }
}
