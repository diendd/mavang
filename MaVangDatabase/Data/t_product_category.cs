﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MaVangDatabase.Data
{
    public class t_product_category
    {
        public int IDProduct { get; set; }
        public int IDCategory { get; set; }

        public m_category Categories { get; set; }
        public m_product  Products { get; set; }
    }
}
