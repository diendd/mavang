﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MaVangDatabase.Data
{
    public class m_category
    {
     
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string CategoryName { get; set; }

        public bool DelFlg { get; set; }

       [ForeignKey("CategoryRefID")]
        public virtual ICollection<m_product> Products { get; set; }
    }
}
