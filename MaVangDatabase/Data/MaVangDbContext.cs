﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace MaVangDatabase.Data
{
    public class MaVangDbContext : IdentityDbContext
    {

        public MaVangDbContext(DbContextOptions<MaVangDbContext> options) : base(options)
        {

        }

        public virtual DbSet<m_category> m_category { get; set; }
        public virtual DbSet<m_product> m_product { get; set; }
        //public virtual DbSet<t_product_category> t_product_category { get; set; }
        public virtual DbSet<t_order> t_order { get; set; }
        public virtual DbSet<t_order_product> t_order_product { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<m_category>()
                .HasMany<m_product>(p => p.Products);
            builder.Entity<t_order_product>().HasKey(t => new { t.IDOrder, t.IDProduct });
            base.OnModelCreating(builder);
        }
    }
}
